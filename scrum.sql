-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 31-05-2023 a las 22:08:14
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `scrum`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogo`
--

CREATE TABLE `catalogo` (
  `nombre` varchar(25) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `catalogo`
--

INSERT INTO `catalogo` (`nombre`, `precio`, `descripcion`) VALUES
('Alpenrauch Everest', 97.97, 'Esta cachimba, diseñada en Austria, es una de las mejores minis del mercado en cuanto a calidades.\r\nLa cámara esta diseñada de estilo tradicional, con un orificio de manguera y varios de purga. '),
('Mamay Coilover', 264.95, 'Es la versión mediana de la marca rusa, que continúa con su impactante diseño.\r\nFabricada en acero inoxidable V2A para gran parte de sus piezas.'),
('Koress K3', 249.00, 'Cuenta con un novedoso sistema de control de la fumada que le permite regular la restricción del tiro en varias posiciones.\r\nEl humo al purgar sale en vertical, por encima de la cámara con un efecto espectacular.'),
('Gorky Hookah', 189.95, 'Con los mejores materiales del mercado y de procedencia rusa, llegan las Gorky Hookah Grey Shisha.'),
('Solomon Hookah', 2450.00, 'Es una verdadera pieza de colección para los grandes amantes de las shishas.\r\nSe fabrica en Rusia en los talleres de la famosa marca JAPONA HOOKAH. La cachimba SOLOMON HOOKAH es un artículo imprescindible para los coleccionistas de shishas de alta gama.'),
('Megatron 2.0', 109.80, 'Es una cachimba que se caracteriza por usar los mejores materiales del mercado combinados con un diseño que le da una gran estética y rendimiento.\r\nTiene un rendimiento tradicional. Posee un tubo de inmersión largo y de anchura media que da una restricción muy agradable.'),
('Mr Rocket', 87.96, 'Una shisha de altura aprox. 42 cm con una base muy estable, su mástil esta hecho de materiales de fibra de carbono, aluminio y cuenta con un tubo de inmersión de acero inoxidable y un difusor incluido.\r\nNuevo sistema de purga desde el centro hacia arriba.'),
('Koress K2', 249.95, 'Llegada para quedarse debido a sus diferencias con el resto, con pequeños detalles pero a la vez novedosos y muy útiles.\r\nCuenta con un novedoso sistema de control que le permite regular el tiro en varias posiciones.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `usuario` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `contraseña` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`usuario`, `apellido`, `email`, `contraseña`) VALUES
('dani', 'dani', 'dani@gmail.com', '1234'),
('fabbs', 'fabbs', 'fabbs@gmail.com', '1234'),
('hola', 'baque', 'tequiero@gmail.com', '1234');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
