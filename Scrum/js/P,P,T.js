// Obtenemos los botones de las opciones
const piedra = document.querySelector("#piedra");
const papel = document.querySelector("#papel");
const tijera = document.querySelector("#tijera");

// Obtenemos el div para mostrar el resultado
const resultado = document.querySelector("#resultado");

// Obtenemos los spans del contador
const victoriasSpan = document.querySelector("#victorias");
const derrotasSpan = document.querySelector("#derrotas");
const empatesSpan = document.querySelector("#empates");

// Inicializamos las variables del contador
let victorias = 0;
let derrotas = 0;
let empates = 0;

// Creamos una función para actualizar el contador
function actualizarContador() {
  victoriasSpan.textContent = victorias;
  derrotasSpan.textContent = derrotas;
  empatesSpan.textContent = empates;
}

// Creamos una función para obtener la opción aleatoria de la computadora
function opcionComputadora() {
  const opciones = ["piedra", "papel", "tijera"];
  const indice = Math.floor(Math.random() * opciones.length);
  return opciones[indice];
}

// Creamos una función para mostrar el resultado
function mostrarResultado(resultadoJuego, opcionUsuario, opcionComp) {
  resultado.innerHTML = `Elegiste ${opcionUsuario}, la computadora eligió ${opcionComp}. ${resultadoJuego}`;
}

// Creamos una función para jugar el juego
function jugar(opcionUsuario) {
  const opcionComp = opcionComputadora();
  let resultadoJuego;

  if (opcionUsuario === opcionComp) {
    resultadoJuego = "Empate!";
    empates++;
  } else if ((opcionUsuario === "piedra" && opcionComp === "tijera") ||
    (opcionUsuario === "papel" && opcionComp === "piedra") ||
    (opcionUsuario === "tijera" && opcionComp === "papel")) {
    resultadoJuego = "Ganaste!";
    victorias++;
  } else {
     resultadoJuego = "Perdiste!";
    derrotas++;
  } 
  actualizarContador();
    mostrarResultado(resultadoJuego, opcionUsuario, opcionComp);
  }
  
  // Agregamos un evento click a cada botón para llamar la función jugar() con la opción elegida por el usuario
    piedra.addEventListener("click", function() {
        jugar("piedra");
  });
  
     papel.addEventListener("click", function() {
        jugar("papel");
  });
  
    tijera.addEventListener("click", function() {
        jugar("tijera");
  });