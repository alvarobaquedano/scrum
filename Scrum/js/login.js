const $btnregistro = document.querySelector(".registro-btn"),
	$btnregistro2 = document.querySelector(".registro2-btn"),
	$registro2 = document.querySelector(".registro2"),
	$registro = document.querySelector(".registro");

document.addEventListener("click", (e) => {
	if (e.target === $btnregistro || e.target === $btnregistro2) {
		$registro.classList.toggle("active");
		$registro2.classList.toggle("active");
	}
});
