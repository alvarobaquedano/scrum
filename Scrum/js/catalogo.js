
  function mostrarProducto(id) {
    // Obtener el producto correspondiente
    var producto = document.getElementById(id);

    // Obtener los datos del producto
    var imagenSrc = producto.querySelector(".imagen img").getAttribute("src");
    var nombre = producto.querySelector(".nombre h3").textContent;
    var precio = producto.querySelector(".nombre span").textContent;
    var descripcion = producto.querySelector("p").textContent;

    // Crear la URL de la página de detalle del producto
    var urlDetalle = "detalle_producto.html";
    urlDetalle += "?imagen=" + imagenSrc;
    urlDetalle += "&nombre=" + encodeURIComponent(nombre);
    urlDetalle += "&precio=" + encodeURIComponent(precio);
    urlDetalle += "&descripcion=" + encodeURIComponent(descripcion);

    // Ir a la página de detalle del producto
    window.location.href = urlDetalle;
  }

  // Agregar el evento de click a los botones "Más información"
  var botonesInfo = document.querySelectorAll(".boton-info button");
  for (var i = 0; i < botonesInfo.length; i++) {
    var botonInfo = botonesInfo[i];
    var producto = botonInfo.parentNode.parentNode;
    var idProducto = producto.getAttribute("id");
    botonInfo.setAttribute("onclick", "mostrarProducto('" + idProducto + "')");
  }

